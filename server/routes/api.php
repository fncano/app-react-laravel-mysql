<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**Categorías */
Route::get('/categorias', 'CategoriesController@index');
Route::get('/categorias/{id}', 'CategoriesController@show');
Route::post('/categorias', 'CategoriesController@store');
Route::put('/categorias/{id}', 'CategoriesController@update');
Route::delete('/categorias/{id}', 'CategoriesController@destroy');
/**Artículos */
Route::get('/articulos', 'ArticlesController@index');
Route::get('/articulos/{id}', 'ArticlesController@show');
Route::post('/articulos', 'ArticlesController@store');
Route::put('/articulos/{id}', 'ArticlesController@update');
Route::delete('/articulos/{id}', 'ArticlesController@destroy');
/**Usuarios */
Route::post('/login', 'UsersController@login');
Route::get('/usuarios', 'UsersController@index');
Route::get('/usuarios/{id}', 'UsersController@show');
Route::post('/usuarios', 'UsersController@store');
Route::put('/usuarios/{id}', 'UsersController@update');
Route::delete('/usuarios/{id}', 'UsersController@destroy');
