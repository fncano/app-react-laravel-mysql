<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use phpDocumentor\Reflection\Types\Nullable;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();

        return response()->json($articles, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = new Article();
        $article->titulo = $request->titulo;
        $article->categoria_id = 1;
        $article->slug = $request->slug;
        $article->texto_largo = $request->texto_largo;
        $article->texto_corto = $request->texto_corto;
        $article->imagen = $request->imagen;
        $article->save();

        return response()->json($article, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);

        return response()->json($article, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::findOrFail($id);
        $article->titulo = $request->titulo;
        $article->categoria_id = $request->categoria_id;
        $article->slug = $request->slug;
        $article->texto_largo = $request->texto_largo;
        $article->texto_corto = $request->texto_corto;
        $article->imagen = $request->imagen;
        $article->update();

        return response()->json($article, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();

        return response()->json($article, 204);
    }
}
