<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    protected $fillable = ['titulo', 'categoria_id', 'slug', 'texto_corto', 'texto_largo', 'imagen', 'created_at', 'updated_at'];

    public function categories() {
        return $this->hasOne('App\Category','categoria_id');
    }
}
