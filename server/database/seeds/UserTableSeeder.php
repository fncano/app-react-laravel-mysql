<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'nombre'=>'Admin User',
            'email'=>'admin@example.com',
            'password'=>'admin',
            'movil'=>'3217654321',
            'rol'=>'administrador',
            'created_at'=>date("Y-m-d"),
            'updated_at'=>date("Y-m-d")
        ], [
            'nombre'=>'Guest User',
            'email'=>'guest@example.com',
            'password'=>'guest',
            'movil'=>'3217654321',
            'rol'=>'usuario',
            'created_at'=>date("Y-m-d"),
        ]])
    }
}
