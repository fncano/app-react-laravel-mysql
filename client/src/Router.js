import React from 'react';
import {  BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'

import Navbar from './components/Navbar';

//import CreateProduct from './components/CreateProduct';
import Login from './components/Login';
import Categorias from './components/Categorias';
import Blog from './components/Blog';
import Usuarios from './components/Usuarios';

const Routes = () => {
    return (
        <Router>
            <Navbar />

            <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/categorias" component={Categorias} />
                <Route exact path="/blog" component={Blog} />
                <Route exact path="/usuarios" component={Usuarios} />
            </Switch>
        </Router>
    )
}

export default Routes;
