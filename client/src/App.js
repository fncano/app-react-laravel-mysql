import React from 'react';
//import logo from './assest/images/logo.svg';
import './assest/css/App.css';

//rutas
import Routes from './Router';

function App() {
  return (
    <div className="App">
      <Routes /> 
    </div>
  );
}

export default App;
