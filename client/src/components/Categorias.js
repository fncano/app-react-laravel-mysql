import React, { Fragment, useState, useEffect } from 'react';
import Axios from 'axios';
import API from '../Environment';

import ListarCategorias from './categorias/ListarCategorias';
import CrearCategoria from './categorias/CrearCategoria';
import EditarCategoria from './categorias/EditarCategoria';

const Categorias = () => {
    //instanciamos nuevos estados
    const [ categorias, setCategorias ] = useState([]);
    const [ categoria, setCategoria ] = useState({})

    useEffect(() => {
        //consultamos los objetos que se encuentran almacenados en la base de datos
        Axios.get(API.url + 'categorias').then(response => {
            setCategorias(response.data)
        }).catch(error => {
            console.log(error);
        })
    }, [])

    //recogemos el objeto que viene desde el elemento padre y los guardamos en la base de datos
    const agregarCategoria = (categoria) => {
        Axios.post(API.url + 'categorias', categoria).then(response => {
            //agregamos el elemento guardado a la lista y actualizando su estado, sin necesida de hcaer una nueva petición al servidor
            setCategorias([ ...categorias, response.data ])
        }).catch(error => {
            console.log(error);
        })
    }

    //con este método recogemos el objecto desde la lista y acrualizamos su estado para se listado en el modal elemento hijo editar
    const editarCategoria = (categoria) => {
        //actualizamos el estado del objeto sin la necesidad de realizar otra consulta al servidor
        setCategoria({ ...categoria, categoria })
    }

    //recogemos el objecto que viene desde el elemento hijo y lo actualizamos
    const actualizarCategoria = (categoria, id) => {
        Axios.put(API.url + 'categorias/' + id, categoria).then(response => {
            const objecto = response.data
            //actualizamos el objecto en la lista sin necesidad de hacer una peticion al servidor
            setCategorias(categorias.map(element => (element.id === objecto.id ? objecto : element)))
        }).catch(error => {
            console.log(error);
        })
    }

    //eliminamos el registro de la base de datos
    const eliminarCategoria = (id) => {
        Axios.delete(API.url + 'categorias/' + id).then(response => {
            //filtramos los objetos de la lista buscamos el objeto eliminado y lo elimnamos de la lista
            //con esta funcion actulizamos la lista de objetos sin necesidad de realizar una nueva petición al servidor
            const array = categorias.filter(categoria => categoria.id !== id)
            setCategorias(array)
        }).catch(error => {
            console.log(error);
        })        
    }

    return (
        <Fragment>
            <div className="content-header">
                <span>Categorías</span>
                <button type="button" className="btn btn-info btn-sm float-right"  data-toggle="modal" data-target="#addModal">
                    Agregar
                </button>
            </div>
            <div className="content-body">
                <ListarCategorias categorias={categorias} editarCategoria={editarCategoria} eliminarCategoria={eliminarCategoria} />
            </div>
            {/**modal agregar */}
            <div className="modal fade" id="addModal" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="addModalLabel">Agregar Categoría</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <CrearCategoria agregarCategoria={agregarCategoria} />
                        </div>
                    </div>
                </div>
            </div>
            {/**modal editar */}
            <div className="modal fade" id="editModal" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="editModalLabel">Editar Categoría</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <EditarCategoria actualizarCategoria={actualizarCategoria} dato={categoria} />
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Categorias;
