import React, { Fragment, useState } from 'react';
import { useHistory } from "react-router-dom"
import { useCookies } from 'react-cookie';
import Axios from 'axios';
import API from '../Environment'

import LoginForm from './login/LoginForm';
import RegistroForm from './login/RegistroForm'

const Login = () => {
    //cookies para guardar el rol del usuario que se registra
    const [cookies, setCookie, removeCookie] = useCookies(['rol']);
    //variable con la cual se muestra el formulario de login o el de registro
    const [ status, setStatus ] = useState(false);
    //redireccionamos a la pagina del blog
    var history = useHistory()

    const eliminarCookie = () => {
        removeCookie('rol')
    }

    const ingresar = (usuario) => {
        //consultamos el usuario que se registra
        Axios.post(API.url + 'login', usuario).then(response => {
            //asigamos de acuerdo al rol una cookie
            if (response.data.rol === 'usuario') {
                setCookie('rol', 2)              
            } else {
                setCookie('rol', 1)                
            }
            //redireccionamos a la pagina del blog
            history.push("/blog")  
        }).catch(error => {
            console.log(error);
        })
    }  

    const agregarUsuario = (usuario) => {
        //agregamos el usario que se registra
        Axios.post(API.url + 'usuarios', usuario).then(response => {            
            //asigamos de acuerdo al rol una cookie
            if (response.data.rol === 'usuario') {
                setCookie('rol', 2)              
            } else {
                setCookie('rol', 1)                
            }
            //redireccionamos a la pagina del blog
            history.push("/blog") 
        }).catch(error => {
            console.log(error);
        })
    }

    return (
        <Fragment>
            <div className="sidenav">
                <div className="login-main-text">
                    <h2>Aplicación</h2>
                    <h2>Página de inicio de sesión</h2>
                    <p>Inicie sesión o registrese</p>
                </div>
            </div>
            <div className="main">
                <div className="row">
                    <div className="col-md-12 col-sm-12">
                        {
                            //de acuerdo al estado de la variable status se muestra el formulario de login o el de registro
                            !status ? (<LoginForm ingresar={ingresar} />) : (<RegistroForm agregarUsuario={agregarUsuario} />)
                        }                        
                    </div>
                    <br></br>
                    <div className="col-md-12 col-sm-12">
                        <br></br>
                        {
                            !status ? (
                                //de acuerdo al estado de la variable status se muestra o no el boton de registro
                                <button className="btn btn-light btn-sm float-right" onClick={() => {setStatus(true)}}>Registrarse</button>
                            ) : (<span></span>)
                        }                        
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Login;
