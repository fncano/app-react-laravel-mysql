import React, { Fragment, useState, useEffect } from 'react';
import Axios from 'axios';
import API from '../Environment';

import ListaUsuarios from './usuarios/ListarUsuarios';
import CrearUsuario from './usuarios/CrearUsuario';
import EditarUsuario from './usuarios/EditarUsuario';

const Usuarios = () => {
    //instanciamos nuevos estados
    const [ usuarios, setUsuarios ] = useState([]);
    const [ usuario, setUsuario ] = useState({})

    useEffect(() => {
        //consultamos los objetos que se encuentran almacenados en la base de datos
        Axios.get(API.url + 'usuarios').then(response => {
            setUsuarios(response.data)
        }).catch(error => {
            console.log(error);
        })
    }, [])

    //recogemos el objeto que viene desde el elemento padre y los guardamos en la base de datos
    const agregarUsuario = (usuario) => {
        Axios.post(API.url + 'usuarios', usuario).then(response => {
            //agregamos el elemento guardado a la lista y actualizando su estado, sin necesida de hcaer una nueva petición al servidor
            setUsuarios([ ...usuarios, response.data ])
        }).catch(error => {
            console.log(error);
        })
    }

    //con este método recogemos el objecto desde la lista y acrualizamos su estado para se listado en el modal elemento hijo editar
    const editarUsuario = (usuario) => {
        //actualizamos el estado del objeto sin la necesidad de realizar otra consulta al servidor
        setUsuario({ ...usuario, usuario })
    }
    //recogemos el objecto que viene desde el elemento hijo y lo actualizamos
    const actualizarUsuario = (usuario, id) => {
        console.log(usuario);
        Axios.put(API.url + 'usuarios/' + id, usuario).then(response => {
            const objecto = response.data
            //actualizamos el objecto en la lista sin necesidad de hacer una peticion al servidor
            setUsuarios(usuarios.map(element => (element.id === objecto.id ? objecto : element)))
        }).catch(error => {
            console.log(error);
        })
    }
    //eliminamos el registro de la base de datos
    const eliminarUsuario = (id) => {
        Axios.delete(API.url + 'usuarios/' + id).then(response => {
            //filtramos los objetos de la lista buscamos el objeto eliminado y lo elimnamos de la lista
            //con esta funcion actulizamos la lista de objetos sin necesidad de realizar una nueva petición al servidor
            const array = usuarios.filter(usuario => usuario.id !== id)
            setUsuarios(array)
        }).catch(error => {
            console.log(error);
        })        
    }

    return (
        <Fragment>
            <div className="content-header">
                <span>Usuarios</span>
                <button type="button" className="btn btn-info btn-sm float-right"  data-toggle="modal" data-target="#addModal">
                    Agregar
                </button>
            </div>
            <div className="content-body">
                <ListaUsuarios usuarios={usuarios} editarUsuario={editarUsuario} eliminarUsuario={eliminarUsuario} />
            </div>
            {/**modal agregar */}
            <div className="modal fade" id="addModal" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="addModalLabel">Agregar Usuario</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <CrearUsuario agregarUsuario={agregarUsuario} />
                        </div>
                    </div>
                </div>
            </div>
            {/**modal editar */}
            <div className="modal fade" id="editModal" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="editModalLabel">Editar Usuario</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <EditarUsuario actualizarUsuario={actualizarUsuario} dato={usuario} />
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Usuarios;
