import React from 'react';

const RegistroForm = (props) => {
    let nombreRef = React.createRef(),
        emailRef = React.createRef(),
        passwordRef = React.createRef(),
        movilRef = React.createRef(),
        rolRef = React.createRef()

    const onSubmit = (event) => {
        event.preventDefault()
        let usuario = {
            nombre: nombreRef.current.value,
            email: emailRef.current.value,
            password: passwordRef.current.value,
            movil: movilRef.current.value,
            rol: rolRef.current.value
        }
        
        props.agregarUsuario(usuario)
        event.target.reset()
    }
    
    return (
        <div className="register-form">
            <form onSubmit={onSubmit}>
                <div className="form-group row">
                    <label htmlFor="nombre" className="col-sm-2 col-form-label">Nombre</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" id="nombre" ref={nombreRef}></input>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="email" className="col-sm-2 col-form-label">Email</label>
                    <div className="col-sm-10">
                        <input type="email" className="form-control" id="email" ref={emailRef}></input>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="password" className="col-sm-2 col-form-label">Password</label>
                    <div className="col-sm-10">
                        <input type="password" className="form-control" id="password" ref={passwordRef}></input>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="movil" className="col-sm-2 col-form-label">Movil</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" id="movil" ref={movilRef}></input>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="rol" className="col-sm-2 col-form-label">Rol</label>
                    <div className="col-sm-10">
                        <select className="form-control" id="rol" ref={rolRef}>
                            <option value="administrador">Administrador</option>
                            <option value="usuario">Usuario</option>
                        </select>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary btn-block">Save changes</button>
            </form>
        </div>
    )
}

export default RegistroForm;