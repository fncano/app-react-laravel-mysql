import React from 'react';

const LoginForm = (props) => {
    //definimos las variables para recoger los valores de los inputs
    let emailRef = React.createRef(), passwordRef = React.createRef();
    //método para enviar los datos del objecto al elemento padre
    const onSubmit = (event) => {
        //evitamos la recarga de la pagina
        event.preventDefault()
        //creamos el objetos y recogemos los valores de cada uno de los inputs, estos valores se asignan a cada propiedad del objeto
        let usuario = {
            email: emailRef.current.value,
            password: passwordRef.current.value
        }
        //por medio de la props se envia el objecto al elemento padre
        props.ingresar(usuario)
        //borramos los datos del formulario
        event.target.reset()
    }
    
    return (
        <div className="login-form">
            <form onSubmit={onSubmit}>
                <div className="form-group">
                    <label>Email</label>
                    <input type="text" className="form-control" placeholder="Email" ref={emailRef}></input>
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Password" ref={passwordRef}></input>
                </div>
                <button type="submit" className="btn btn-primary btn-block">Ingresar</button>
            </form>
        </div>
    )
}

export default LoginForm;
