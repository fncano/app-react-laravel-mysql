import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import { useCookies } from 'react-cookie';

const Navbar = () => {   
    //obtenemos el valor de la cookie almacenada
    const [ cookies, removeCookie ] = useCookies(['rol']);
    //eliminamos el valor de la cookie almacenada
    const eliminarCookie = () => {
        removeCookie('rol')
    }

    return (        
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            {
                //las opciones del navbar se muestran si viene alguno de los valores se 1 ó 2
                cookies.rol === '1' || cookies.rol === '2' ? (
                    <Fragment>
                        <a className="navbar-brand" href="/blog">Navbar</a>
                        {
                            //si el usuario es administrador se muestran estas opciones
                            cookies.rol === '1' ? (                        
                                <ul className="navbar-nav mr-auto">
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/login" activeClassName="active" onClick={eliminarCookie()}>Inicio</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/categorias" activeClassName="active">Categorías</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/blog" activeClassName="active">Blog</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/usuarios" activeClassName="active">Usuarios</NavLink>
                                    </li>
                                </ul>
                            ) : (
                                //si el usuario es estandar se muestra solo esta opción
                                <ul className="navbar-nav mr-auto">
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/blog" activeClassName="active">Blog</NavLink>
                                    </li>
                                </ul>
                            )
                            //boton salir, se elimina la cookie
                        }
                        <form className="form-inline">                        
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item">
                                    <NavLink className="nav-link" to="/" onClick={eliminarCookie()}>Salir</NavLink>
                                </li>
                            </ul>
                        </form>
                    </Fragment>
                ) : ( <a className="navbar-brand" href="/login" onClick={eliminarCookie()}>Navbar</a> )
            }
            
        </nav>   
    )
}

export default Navbar;
