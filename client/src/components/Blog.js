import React, { Fragment, useState, useEffect } from 'react';
import Axios from 'axios';
import API from '../Environment';

import ListaArticulos from './articulos/ListarArticulos';
import CrearArticulo from './articulos/CrearArticulo';
import EditarArticulo from './articulos/EditarArticulo';
import { useCookies } from 'react-cookie';

const Articulos = () => {
    //instanciamos nuevos estados
    const [ articulos, setArticulos ] = useState([])
    const [ categorias, setCategorias ] = useState([])
    const [ articulo, setArticulo ] = useState({})    
    const [cookies, setCookie, removeCookie] = useCookies(['rol']);

    const procesarRespuesta = (data, opcion) => {
        if (opcion === 1) {
            for (let i = 0; i < data.length; i++) {
                const e = data[i];
                //consultamos la categoría que pertenece a cada artículo
                Axios.get(API.url + 'categorias/' + e.categoria_id).then(response => { 
                    //se crea una nueva propiedad en el objeto de la lista y se le asigna el nombre de la categoría
                    Object.defineProperty(e, 'categoria_nombre', {
                        enumerable: false,
                        configurable: false,
                        writable: false,
                        value: response.data.nombre
                      });
                    //en la ultima iteración actualizamos el estado de la lista de objetos
                    if ((i + 1) === data.length) {
                        setArticulos(data)                        
                    }
                }).catch(error => {
                    console.log(error);
                }) 
            }            
        } else {
            Axios.get(API.url + 'categorias/' + data.categoria_id).then(response => { 
                console.log(response.data.nombre);
                //se crea una nueva propiedad en el objeto de la lista y se le asigna el nombre de la categoría
                Object.defineProperty(data, 'categoria_nombre', {
                    enumerable: false,
                    configurable: false,
                    writable: false,
                    value: response.data.nombre
                });
                //actualizamos el estado de la lista de objetos
                switch (opcion) {
                    case 2:
                        setArticulos([ ...articulos, data ])                        
                        break;
                    
                    case 3:                        
                    //recorremos el array de la lista y buscamos el objecto sin actualizar y lo actualizamos, de esta forma actualizamos
                    //el estado de la lista sin realizar una nueva petición al servidor
                    setArticulos(articulos.map(element => (element.id === data.id ? data : element)))
                        break;
                
                    default:
                        break;
                }
            }).catch(error => {
                console.log(error);
            })
        }
    }

    useEffect(() => {
        //consultamos los objetos que se encuentran almacenados en la base de datos
        Axios.get(API.url + 'articulos').then(response => {
            //con esta función buscamos la categoria asociada a cada registro de articulos
            procesarRespuesta(response.data, 1)
        }).catch(error => {
            console.log(error);
        })    

        Axios.get(API.url + 'categorias').then(response => {
            setCategorias(response.data)
        }).catch(error => {
            console.log(error);
        })
    }, [])
    //recogemos el objeto que viene desde el elemento padre y los guardamos en la base de datos
    const agregarArticulo = (articulo) => {
        Axios.post(API.url + 'articulos', articulo).then(response => {
            procesarRespuesta(response.data, 2)
        }).catch(error => {
            console.log(error);
        })
    }
    //con este método recogemos el objecto desde la lista y acrualizamos su estado para se listado en el modal elemento hijo editar
    const editarArticulo = (articulo) => {
        //actualizamos el estado del objeto sin la necesidad de realizar otra consulta al servidor
        setArticulo({ ...articulo, articulo })
    }
    //recogemos el objecto que viene desde el elemento hijo y lo actualizamos

    const actualizarArticulo = (articulo, id) => {
        //enviamos el dato al servidor para actalizarlo
        Axios.put(API.url + 'articulos/' + id, articulo).then(response => {
            procesarRespuesta(response.data, 3)
        }).catch(error => {
            console.log(error);
        })
    }

    const eliminarArticulo = (id) => {
        //enviamos el dato al servidor para eliminarlo
        Axios.delete(API.url + 'articulos/' + id).then(response => {
            //filtramos los objetos de la lista buscamos el objeto eliminado y lo elimnamos de la lista
            //con esta funcion actulizamos la lista de objetos sin necesidad de realizar una nueva petición al servidor
            const array = articulos.filter(articulo => articulo.id !== id)
            setArticulos(array)
        }).catch(error => {
            console.log(error);
        })        
    }

    return (
        <Fragment>
            <div className="content-header">
                <span>Artículos</span>
                {
                    cookies.rol === '1' ? (

                        <button type="button" className="btn btn-info btn-sm float-right"  data-toggle="modal" data-target="#addModal">
                            Agregar
                        </button>
                    ) : ( <span></span> )
                }
            </div>
            <div className="content-body">
                <ListaArticulos articulos={articulos} editarArticulo={editarArticulo} eliminarArticulo={eliminarArticulo} />
            </div>
            {/**modal agregar */}
            <div className="modal fade" id="addModal" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="addModalLabel">Agregar Artículo</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <CrearArticulo agregarArticulo={agregarArticulo} categorias={categorias} />
                        </div>
                    </div>
                </div>
            </div>
            {/**modal editar */}
            <div className="modal fade" id="editModal" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="editModalLabel">Editar Artículo</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <EditarArticulo actualizarArticulo={actualizarArticulo} categorias={categorias} dato={articulo} />
                        </div>
                    </div>
                </div>
            </div>
            {/**modal información */}
            <div className="modal fade" id="infoModal" role="dialog" aria-labelledby="infoModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="infoModalLabel">Más información</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>{articulo.categoria_nombre}</p>   
                            <p>{articulo.titulo}</p> 
                            <p>{articulo.texto_corto}</p> 
                            <p>{articulo.texto_largo}</p>                 
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Articulos;
