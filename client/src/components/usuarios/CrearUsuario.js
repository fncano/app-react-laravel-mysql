import React from 'react';

const CrearUsuario = (props) => {
    //definimos las variables para recoger los valores de los inputs
    let nombreRef = React.createRef(),
        emailRef = React.createRef(),
        passwordRef = React.createRef(),
        movilRef = React.createRef(),
        rolRef = React.createRef()

    //método para enviar los datos del objecto al elemento padre
    const onSubmit = (event) => {
        //evitamos la recarga de la pagina
        event.preventDefault()
        //creamos el objetos y recogemos los valores de cada uno de los inputs, estos valores se asignan a cada propiedad del objeto
        let usuario = {
            nombre: nombreRef.current.value,
            email: emailRef.current.value,
            password: passwordRef.current.value,
            movil: movilRef.current.value,
            rol: rolRef.current.value
        }
        
        //por medio de la props se envia el objecto al elemento padre
        props.agregarUsuario(usuario)
        //borramos los datos del formulario
        event.target.reset()
    }
    
    return (
        <form onSubmit={onSubmit}>
            <div className="form-group row">
                <label htmlFor="nombre" className="col-sm-2 col-form-label">Nombre</label>
                <div className="col-sm-10">
                    <input type="text" className="form-control" id="nombre" ref={nombreRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="email" className="col-sm-2 col-form-label">Email</label>
                <div className="col-sm-10">
                    <input type="email" className="form-control" id="email" ref={emailRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="password" className="col-sm-2 col-form-label">Password</label>
                <div className="col-sm-10">
                    <input type="password" className="form-control" id="password" ref={passwordRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="movil" className="col-sm-2 col-form-label">Movil</label>
                <div className="col-sm-10">
                    <input type="text" className="form-control" id="movil" ref={movilRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="rol" className="col-sm-2 col-form-label">Rol</label>
                <div className="col-sm-10">
                    <select className="form-control" id="rol" ref={rolRef}>
                        <option value="administrador">Administrador</option>
                        <option value="usuario">Usuario</option>
                    </select>
                </div>
            </div>
            <button type="submit" className="btn btn-primary btn-block">Save changes</button>
        </form>
    )
}

export default CrearUsuario;
