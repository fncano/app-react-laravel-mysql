import React from 'react';

const EditarUsuario = (props) => {
    //definimos las variables para recoger los valores de los inputs
    let nombreRef = React.createRef(),
        emailRef = React.createRef(),
        passwordRef = React.createRef(),
        movilRef = React.createRef(),
        rolRef = React.createRef()

    //método para enviar los datos del objecto al elemento padre
    const onSubmit = (event) => {
        //evitamos la recarga de la pagina
        event.preventDefault()
        //creamos el objetos y recogemos los valores de cada uno de los inputs, estos valores se asignan a cada propiedad del objeto
        let usuario = {
            id: props.dato.id,
            nombre: nombreRef.current.value,
            email: emailRef.current.value,
            password: passwordRef.current.value,
            movil: movilRef.current.value,
            rol: rolRef.current.value,
            created_at: props.dato.created_at,
            updated_at: new Date()
        }

        //por medio de la props se envia el objecto al elemento padre
        props.actualizarUsuario(usuario, props.dato.id)
        //borramos los datos del formulario
        event.target.reset()

        //los valores que se recogen desde el elemento padre por medio de la props se asigan a cada input por medio del la prop defaultValue
    }
    
    return (
        <form onSubmit={onSubmit}>
            <div className="form-group row">
                <label htmlFor="e-nombre" className="col-sm-2 col-form-label">Nombre</label>
                <div className="col-sm-10">
                    <input type="text" className="form-control" id="e-nombre" defaultValue={props.dato.nombre} ref={nombreRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="e-email" className="col-sm-2 col-form-label">Email</label>
                <div className="col-sm-10">
                    <input type="email" className="form-control" id="e-email" defaultValue={props.dato.email} ref={emailRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="e-password" className="col-sm-2 col-form-label">Password</label>
                <div className="col-sm-10">
                    <input type="password" className="form-control" id="e-password" defaultValue={props.dato.password} ref={passwordRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="e-movil" className="col-sm-2 col-form-label">Movil</label>
                <div className="col-sm-10">
                    <input type="text" className="form-control" id="e-movil" defaultValue={props.dato.movil} ref={movilRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="e-rol" className="col-sm-2 col-form-label">Rol</label>
                <div className="col-sm-10">
                    <select className="form-control" id="e-rol" defaultValue={props.dato.rol} ref={rolRef}>
                        <option value="administrador">Administrador</option>
                        <option value="usuario">Usuario</option>
                    </select>
                </div>
            </div>
            <p><small>Por seguridad el password no se muestra, si quieres actualizarlo puedes ingresar el nuevo password</small></p>
            <button type="submit" className="btn btn-primary btn-block">Save changes</button>
        </form>
    )
}

export default EditarUsuario;
