import React from 'react';

const ListaUsuarios = (props) => {
    //recogemos la lista que viene del elemento padre por medio de las props
    let usuarios = props.usuarios;
    //los botonoes editar y eliminar llaman los métodos correspondientes en el elmento padre

    return (
        <table className="table">
            <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Movil</th>
                    <th scope="col">Rol</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                {usuarios.length > 0 ?
                    (usuarios.map((item, index) => (
                        <tr key = {index}>
                            <th scope="row">{index + 1}</th>
                            <td>{item.nombre}</td>
                            <td>{item.email}</td>
                            <td>{item.movil}</td>
                            <td>{item.rol}</td>
                            <td>
                                <button className="btn btn-info btn-sm" onClick={() => {props.editarUsuario(item)}} data-toggle="modal" data-target="#editModal">Editar</button>
                                &nbsp;&nbsp;
                                <button className="btn btn-danger btn-sm" onClick={() => {props.eliminarUsuario(item.id)}}>Eliminar</button>
                            </td>
                        </tr>
                    ))) :
                    (<tr><td colSpan={6}>No hay usuarios</td></tr>)
                }
            </tbody>
        </table>
    )
}

export default ListaUsuarios;
