import React, { useState } from 'react';

const CrearArticulo = (props) => {
    const [ opcion, setOpcion ]  = useState('')
    //definimos las variables para recoger los valores de los inputs
    let tituloRef = React.createRef(),
        slugRef = React.createRef(),
        tcortoRef = React.createRef(),
        tlargoRef = React.createRef()    

    const onChangeRadio = (id) => {
        setOpcion(id);
    }
    //método para enviar los datos del objecto al elemento padre
    const onSubmit = (event) => {
        //evitamos la recarga de la pagina
        event.preventDefault()
        //creamos el objetos y recogemos los valores de cada uno de los inputs, estos valores se asignan a cada propiedad del objeto
        let articulo = {
            titulo: tituloRef.current.value,
            categoria_id: opcion,
            slug: slugRef.current.value,
            texto_corto: tcortoRef.current.value,
            texto_largo: tlargoRef.current.value,
            imagen: 'prueba'
        }     

        //por medio de la props se envia el objecto al elemento padre
        props.agregarArticulo(articulo)
        //borramos los datos del formulario
        event.target.reset()
    }
    
    return (
        <form onSubmit={onSubmit}>
            <div className="form-group row">
                <label htmlFor="titulo" className="col-sm-2 col-form-label">Título</label>
                <div className="col-sm-10">
                    <input type="text" className="form-control" id="titulo" ref={tituloRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="categoria" className="col-sm-2 col-form-label">Categoría</label>
                <div className="col-sm-10">
                    <div className="c-content">
                    {props.categorias.length > 0 ?
                    (props.categorias.map((item, index) => (
                        
                        <div className="form-check" key={index}>
                            <input className="form-check-input" type="radio" name="exampleRadios" id={'radio'+index} onClick={() => {onChangeRadio(item.id)}} value={item.id}></input>
                            <label className="form-check-label" htmlFor={'radio'+index}>
                                {item.nombre}
                            </label>
                        </div>
                    ))) :
                    (<span>No hay categorías registradas</span>)
                }
                    </div>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="slug" className="col-sm-2 col-form-label">Slug</label>
                <div className="col-sm-10">
                    <input type="text" className="form-control" id="slug" ref={slugRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="tcorto" className="col-sm-2 col-form-label">Texto corto</label>
                <div className="col-sm-10">
                    <textarea className="form-control" id="tcorto" ref={tcortoRef}></textarea>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="tlargo" className="col-sm-2 col-form-label">Texto largo</label>
                <div className="col-sm-10">                
                    <textarea className="form-control" id="tlargo" ref={tlargoRef}></textarea>
                </div>
            </div>
            <button type="submit" className="btn btn-primary btn-block">Save changes</button>
        </form>
    )
}

export default CrearArticulo;
