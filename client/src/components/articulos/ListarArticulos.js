import React from 'react';
import { useCookies } from 'react-cookie';

const ListaArticulos = (props) => {
    //recogemos la el valor asignado en la cookie lo que nos permite mostrar los boteones de este
    //modúlo de acuerdo al usuario logueado, el administrador ve los botones del CRUD y usuario solo ve el boton más detalles
    const [ cookies ] = useCookies(['rol']);
    //recogemos la lista que viene del elemento padre por medio de las props
    let articulos = props.articulos;
    //los botonoes editar y eliminar llaman los métodos correspondientes en el elmento padre

    return (
        <table className="table">
            <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Categoría</th>
                    <th scope="col">Título</th>
                    <th scope="col">Slug</th>
                    <th scope="col">Texto corto</th>
                    <th scope="col">Texto Largo</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                {articulos.length > 0 ?
                    (articulos.map((item, index) => (
                        <tr key = {index}>
                            <th scope="row">{index + 1}</th>
                            <td>{item.categoria_nombre}</td>
                            <td>{item.titulo}</td>
                            <td>{item.slug}</td>
                            <td>{item.texto_corto}</td>
                            <td>{item.texto_largo}</td>
                            <td>
                                {
                                    cookies.rol === '1' ? (
                                        <div>
                                            <button className="btn btn-info btn-sm" onClick={() => {props.editarArticulo(item)}} data-toggle="modal" data-target="#editModal">Editar</button>
                                            &nbsp;&nbsp;
                                            <button className="btn btn-danger btn-sm" onClick={() => {props.eliminarArticulo(item.id)}}>Eliminar</button>
                                        </div>
                                    ) : (
                                        <button className="btn btn-info btn-sm" onClick={() => {props.editarArticulo(item)}} data-toggle="modal" data-target="#infoModal">Más información</button>
                                    )
                                }
                                
                            </td>
                        </tr>
                    ))) :
                    (<tr><td colSpan={6}>No hay articulos</td></tr>)
                }
            </tbody>
        </table>
    )
}

export default ListaArticulos;
