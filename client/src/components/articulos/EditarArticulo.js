import React, { useState } from 'react';

const EditarArticulo = (props) => {
    const [ opcion, setOpcion ]  = useState('')

    //definimos las variables para recoger los valores de los inputs
    let tituloRef = React.createRef(),
        //categoriaRef = React.createRef(),
        slugRef = React.createRef(),
        tcortoRef = React.createRef(),
        tlargoRef = React.createRef() 
    //con este método recogemos el valor que se marque en los radio button
    const onChangeRadio = (id) => {
        setOpcion(id);
    }

    //método para enviar los datos del objecto al elemento padre
    const onSubmit = (event) => {
        //evitamos la recarga de la pagina
        event.preventDefault()
        let articulo = {
            id: props.dato.id,
            titulo: tituloRef.current.value,
            categoria_id: opcion !== '' ? opcion : props.dato.categoria_id,
            slug: slugRef.current.value,
            texto_corto: tcortoRef.current.value,
            texto_largo: tlargoRef.current.value,
            imagen: 'img',
            created_at: props.dato.created_at,
            updated_at: new Date()
        }

        //por medio de la props se envia el objecto al elemento padre
        props.actualizarArticulo(articulo, props.dato.id)
        //borramos los datos del formulario
        event.target.reset()

        //los valores que se recogen desde el elemento padre por medio de la props se asigan a cada input por medio del la prop defaultValue
    }
    
    return (
        <form onSubmit={onSubmit}>
            <div className="form-group row">
                <label htmlFor="titulo" className="col-sm-2 col-form-label">Título</label>
                <div className="col-sm-10">
                    <input type="text" className="form-control" id="titulo" defaultValue={props.dato.titulo} ref={tituloRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="categoria" className="col-sm-2 col-form-label">Categoría</label>
                <div className="col-sm-10">
                    <select className="form-control" id="categoria" value={props.dato.categoria_id} disabled={true}>
                        {
                            props.categorias.map((item, index) => (
                                <option key={index} value={item.id}>{item.nombre}</option>
                            ))            
                        }
                    </select>
                </div>
            </div>            
            <div className="form-group row">
                <label htmlFor="categoria" className="col-sm-2 col-form-label">Nueva categoría</label>
                <div className="col-sm-10">
                    <div className="c-content">
                        {props.categorias.length > 0 ?
                            (props.categorias.map((item, index) => (                        
                                <div className="form-check" key={index}>
                                    <input className="form-check-input" type="radio" name="eRadios" id={'e-radio'+index} onClick={() => {onChangeRadio(item.id)}} value={item.id}></input>
                                    <label className="form-check-label" htmlFor={'e-radio'+index}>
                                        {item.nombre}
                                    </label>
                                </div>
                            ))) :
                            (<span>No hay categorías registradas</span>)
                        }
                    </div>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="slug" className="col-sm-2 col-form-label">Slug</label>
                <div className="col-sm-10">
                    <input type="text" className="form-control" id="slug" defaultValue={props.dato.slug} ref={slugRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="tcorto" className="col-sm-2 col-form-label">Texto corto</label>
                <div className="col-sm-10">
                    <textarea className="form-control" id="tcorto" defaultValue={props.dato.texto_corto} ref={tcortoRef}></textarea>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="tlargo" className="col-sm-2 col-form-label">Texto largo</label>
                <div className="col-sm-10">
                    <textarea className="form-control" id="tlargo" defaultValue={props.dato.texto_largo} ref={tlargoRef}></textarea>
                </div>
            </div>
            <button type="submit" className="btn btn-primary btn-block">Save changes</button>
        </form>
    )
}

export default EditarArticulo;
