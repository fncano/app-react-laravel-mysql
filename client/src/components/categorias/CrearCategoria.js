import React from 'react';

const CrearCategoria = (props) => {
    //definimos las variables para recoger los valores de los inputs
    let nombreRef = React.createRef(), descripcionRef = React.createRef();
    //método para enviar los datos del objecto al elemento padre
    const onSubmit = (event) => {
        //evitamos la recarga de la pagina
        event.preventDefault()
        //creamos el objetos y recogemos los valores de cada uno de los inputs, estos valores se asignan a cada propiedad del objeto
        let categoria = {
            nombre: nombreRef.current.value,
            descripcion: descripcionRef.current.value
        }
        //por medio de la props se envia el objecto al elemento padre        
        props.agregarCategoria(categoria)
        //borramos los datos del formulario
        event.target.reset()
    }
    
    return (
        <form onSubmit={onSubmit}>
            <div className="form-group row">
                <label htmlFor="nombre" className="col-sm-2 col-form-label">Nombre</label>
                <div className="col-sm-10">
                    <input type="text" className="form-control" id="nombre" ref={nombreRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="descripcion" className="col-sm-2 col-form-label">Descipción</label>
                <div className="col-sm-10">
                    <textarea className="form-control" id="descripcion" ref={descripcionRef}></textarea>
                </div>
            </div>
            <button type="submit" className="btn btn-primary btn-block">Save changes</button>
        </form>
    )
}

export default CrearCategoria;
