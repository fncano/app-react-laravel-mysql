import React from 'react';

const EditarCategoria = (props) => {
    //definimos las variables para recoger los valores de los inputs
    let nombreRef = React.createRef(), descripcionRef = React.createRef();

    //método para enviar los datos del objecto al elemento padre
    const onSubmit = (event) => {
        //evitamos la recarga de la pagina
        event.preventDefault()
        //creamos el objetos y recogemos los valores de cada uno de los inputs, estos valores se asignan a cada propiedad del objeto
        let categoria = {
            id: props.dato.id,
            nombre: nombreRef.current.value,
            descripcion: descripcionRef.current.value,
            created_at: props.dato.created_at,
            updated_at: new Date()
        }

        //por medio de la props se envia el objecto al elemento padre
        props.actualizarCategoria(categoria, props.dato.id)
        //borramos los datos del formulario
        event.target.reset()

        //los valores que se recogen desde el elemento padre por medio de la props se asigan a cada input por medio del la prop defaultValue
    }
    
    return (
        <form onSubmit={onSubmit}>
            <div className="form-group row">
                <label htmlFor="nombre" className="col-sm-2 col-form-label">Nombre</label>
                <div className="col-sm-10">
                    <input type="text" className="form-control" id="nombre" defaultValue={props.dato.nombre} ref={nombreRef}></input>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="descripcion" className="col-sm-2 col-form-label">Descipción</label>
                <div className="col-sm-10">
                    <textarea className="form-control" id="descripcion" defaultValue={props.dato.descripcion} ref={descripcionRef}></textarea>
                </div>
            </div>
            <button type="submit" className="btn btn-primary btn-block">Save changes</button>
        </form>
    )
}

export default EditarCategoria;
