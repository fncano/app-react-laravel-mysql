import React from 'react';

const ListaCategorias = (props) => {
    //recogemos la lista que viene del elemento padre por medio de las props
    let categorias = props.categorias;
    //los botonoes editar y eliminar llaman los métodos correspondientes en el elmento padre

    return (
        <table className="table">
            <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Descripción</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                {categorias.length > 0 ?
                    (categorias.map((item, index) => (
                        <tr key = {index}>
                            <th scope="row">{index + 1}</th>
                            <td>{item.nombre}</td>
                            <td>{item.descripcion}</td>
                            <td>
                                <button className="btn btn-info btn-sm" onClick={() => {props.editarCategoria(item)}} data-toggle="modal" data-target="#editModal">Editar</button>
                                &nbsp;&nbsp;
                                <button className="btn btn-danger btn-sm" onClick={() => {props.eliminarCategoria(item.id)}}>Eliminar</button>
                            </td>
                        </tr>
                    ))) :
                    (<tr><td colSpan={4}>No hay categorías</td></tr>)
                }
            </tbody>
        </table>
    )
}

export default ListaCategorias;
